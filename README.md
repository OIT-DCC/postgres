# postgres

Singularity/Apptainer container from postgres-alpine docker image. This project is a copy of https://github.com/verysure/postgres-alpine.

Pulling Built Image for Use
---------------------------
The Singularity/Apptainer image created is stored on research-singularity-registry.oit.duke.edu. You can pull the image down by curl (e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/postgres.sif).

Usage
-----
Example:

* Create a location to store the PostgreSQL data files so that they can be reused. e.g. mkdir /hpc/group/oit/postgresql and /hpc/group/oit/postgresql/data
* Create a configuration file that will contain the name of the database, the password for the postgres root user, and the host/port information i.e. create/edit /hpc/group/cosmology/postgresql/postgresrc with content like:
   ```
   export PGPASSWORD=mypgpassword
   export PGDATABASE=pgdemo
   export PGHOSTADDR=127.0.0.1:5432
   ```
* Starting the PostgreSQL container instance will initialize the data files and make the database ready for use:
  ```
  apptainer instance start -B /hpc/group/oit/postgresql/postgresrc:/postgresrc -B /hpc/group/oit/postgresql/data:/var/lib/postgresql/data -B /var/tmp:/var/run /opt/apps/containers/oit/postgres/postgres.sif mypginstance
  ```
* Create tables, insert and select data etc.
  ```
  apptainer exec /opt/apps/containers/oit/postgres/postgres.sif psql -d pgdemo -h localhost -p 5432 -U postgres -c "select 1;"
  ```
* When finished shutdown the container instance:
  ```
  apptainer instance stop mypginstance
  ```


Building Manually
-----------------

To build the image, run `apptainer build <name.sif> Singularity`.
See [Apptainer](https://apptainer.org/docs/user/latest/)
for more info.


Acknowledge
-----------

The recipes build from many open source projects, including
* [Apptainer](https://apptainer.org/docs/user/latest/)
* [postgres](https://www.postgresql.org/)
* [Alpine Linux](https://www.alpinelinux.org/)
